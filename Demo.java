import java.io.*;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Demo - класс, работающий с регулярными выражениями.
 */

public class Demo {
    public static void main(String[] args) throws IOException {
        //URL страницы, из которой необходимо вытащить Email
        URL url = new URL("http://www.penzainform.ru/");

        //Создаём новый файл для записи вытащенных Email
        BufferedWriter outFile = new BufferedWriter(new FileWriter("outFile.txt"));

        String result;

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            result = bufferedReader.lines().collect(Collectors.joining("\n"));
        }

        //Email-адрес в формате xxx@xxxx.xxx (не игнорирует регистр)
        Pattern email_pattern = Pattern.compile("[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})");

        //Matcher — класс, который представляет строку, реализует механизм согласования (matching).

        Matcher matcher = email_pattern.matcher(result);

/**
 * matcher.find() - метод возвращает true только тогда, когда вся строка соответствует заданному регулярному выражению.
 */
        //Запись в файл извлечённых Email.
        while (matcher.find()) {
            outFile.write(matcher.group() + "\r\n");
        }
        outFile.close();
    }
}