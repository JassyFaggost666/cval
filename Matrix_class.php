<meta charset="UTF-8">

<?php

/**
 * Class Matrix
 * Класс для создания объекта матрицы и работа с методами матрицы
 */
class Matrix
{
    private $length;
    private $width;
    private $array;

    /**
     * Matrix constructor.
     * @param $length - высота матрицы
     * @param $width - ширина матрицы
     */
    public function __construct($length, $width)
    {
        $this->array = array();
        $this->length = $length;
        $this->width = $width;
    }

    /**
     * Matrix constructor 2.
     * @param $array - массив который последовательно будет помещён в матрицу
     * @param $length -  высота матрицы
     * @param $width - ширина матрицы
     */
    public function __construct1($array, $length, $width)
    {
        $this->array = $array;
        $this->length = $length;
        $this->width = $width;
    }

    /**
     * Matrix constructor(default)
     * length - рандомное число от 1 до 10
     * width - рандомное число от 1 до 10
     */
    public function __construct2()
    {
        $this->array = array();
        $this->length = rand(1, 10);
        $this->width = rand(1, 10);
    }

    /**
     * Рандомное заполнение массива с указанным диапазоном
     * @param $min - минимальное значение диапазона
     * @param $max - максимальное значение диапазона
     */
    function fillingRandValues($min, $max)
    {
        for ($i = 0; $i < $this->length; $i++) {
            for ($j = 0; $j < $this->width; $j++) {
                $this->array[$i][$j] = rand($min, $max);
            }
        }
    }

    /**
     *
     * @param $line
     * @param $column
     * @param $value
     */
    function setValue($line, $column, $value)
    {
        for ($i = 0; $i < $this->length; $i++) {
            for ($j = 0; $j < $this->width; $j++) {
                $this->array[$line][$column] = $value;
            }
        }
    }

    /**
     * @param $line
     * @param $column
     * @return mixed
     */
    function getValue($line, $column)
    {
        for ($i = 0; $i < $this->length; $i++) {
            for ($j = 0; $j < $this->width; $j++) {
                return $this->array[$line][$column];
            }
        }
    }

    /**
     *  Метод вывода матрицы на экран
     */
    function print_matrix()
    {
        echo "<table border=\"1\">";
        for ($i = 0; $i < $this->length; $i++) {
            echo "<tr>";
            for ($j = 0; $j < $this->width; $j++) {
                echo "<td>" . $this->array[$i][$j] . "</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }


    /**
     * Метод находит среднее арифметическое матрицы
     *
     * @return float|int - среднее арифметическое матрицы
     */
    function average()
    {
        $sum = 0;
        for ($i = 0; $i < $this->length; $i++) {
            for ($j = 0; $j < $this->width; $j++) {
                $sum = +$this->array[$i][$j];
            }
        }
        return $sum / count($this->array);
    }

    /**
     * Нахождение максимального значения в матрице
     * @return mixed - максимальное значение
     */
    function maxValue()
    {
        $max = $this->array[0][0];
        for ($i = 0; $i < $this->length; $i++) {
            for ($j = 0; $j < $this->width; $j++) {
                if ($this->array[$i][$j] > $max) {
                    $max = $this->array[$i][$j];
                }
            }
        }
        return $max;
    }

    /**
     * Нахождение минимального значения в матрице
     * @return mixed - максимальное значение
     */
    function minValue()
    {
        $min = $this->array[0][0];
        for ($i = 0; $i < $this->length; $i++) {
            for ($j = 0; $j < $this->width; $j++) {
                if ($this->array[$i][$j] < $min) {
                    $min = $this->array[$i][$j];
                }
            }
        }
        return $min;
    }

    /**
     * Находит среднее арефметическое методом (max+min)/2
     * @return float|int - среднее арефметическое
     */
    function averageWithMinAndMaxValue()
    {
        return (Matrix::maxValue() + Matrix::minValue()) / 2;
    }

    /**
     * Находит разницу между методами нахождения среднего арефметического
     * @return int - разница между методами нахождения среднего арефметического
     */
    function correlationMatrix()
    {
        return Matrix::average() - Matrix::averageWithMinAndMaxValue();
    }

    /**
     *  Находит определитель матрицы
     * @return int - определитель матрицы
     */
    function determinant()
    {
        if ($this->length == 1) {
            return $this->array[0][0];
        }
        if ($this->length == 2) {
            return ($this->array[0][0] * $this->array[1][1] - $this->array[0][1] * $this->array[1][0]);
        }
        if ($this->length == 3) {
            return (($this->array[0][0] * $this->array[1][1] * $this->array[2][2] + $this->array[0][1] * $this->array[1][2]
                    * $this->array[2][0] + $this->array[0][2] * $this->array[1][0] * $this->array[2][1])
                - ($this->array[0][0] * $this->array[1][2] * $this->array[2][1] + $this->array[0][1] * $this->array[1][0] *
                    $this->array[2][2] + $this->array[0][2] * $this->array[1][1] * $this->array[2][0]));
        }
        return "В доработке";
    }

    /**
     *
     * @return string
     */
    function diagonalDifference()
    {
        $pow = 1;
        for ($i = 0; $i < $this->length; $i++) {
            $pow *= $this->array[$i][$i];
        }
        $pow2 = 1;
        for ($i = $this->length; $i > 0; $i--) {
            $pow2 *= $this->array[$i][$i];
        }
        if ($pow > $pow2) {
            return "Больше произведение главной диагонали";
        } else {
            return "Больше произведение побочной диагонали";
        }

    }

    /**
     *
     * @param $char
     */
    function fillingZeroesDiagonals($char)
    {
        $countMatrix = 0;
        while ($countMatrix <= $this->length / $this->width) {
            $i = $this->width * $countMatrix - $countMatrix;
            $j = 0;
            while ($i < $this->width * ($countMatrix + 1) && $j < $this->width) {
                $this->array[$i][$j] = $char;
                $i++;
                $j++;
            }
            $countMatrix++;
            $i = $this->width * $countMatrix - $countMatrix;
            $j = $this->width - 1;
            while ($i < $this->width * ($countMatrix + 1) && $j >= 0) {
                $this->array[$i][$j] = $char;
                $i++;
                $j--;
            }
            $countMatrix++;
        }
    }

    /**
     * Поворот матрицы на 90 градусов
     * @return Matrix матрицу, которую перевернули
     */
    function rotateNinetyDegreesClockwise()
    {
        $temp_matrix = array();
        for ($i = 0; $i < $this->length; $i++) {
            for ($j = 0; $j < $this->width; $j++) {
                $temp_matrix[$j][$i] = $this->array[$i][$j];
            }
        }
        return new Matrix($temp_matrix, $this->width, $this->length);
    }

}